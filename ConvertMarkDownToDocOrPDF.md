# Convert MarkDown file to Doc or Pdf  File

* install Pandoc
`apt-get install pandoc`

* To convert a file.md Markdown file into a PDF document
`pandoc -s -o file.pdf file.md`

* To convert multiple Markdown file into a PDF document
`pandoc -s -o doc.pdf part01.md part02.md`


Pandoc is able to merge multiple Markdown files into a single PDF document. To generate a single PDF document out of two Markdown files you can use:
By default the page margins in the resulting PDF document are quite large. You can change this by passing a margin parameter:

pandoc -s -V geometry:margin=1in -o documentation.pdf part01.md part02.md
To create HTML or DOCX documents you simply have to change the file extension of the target file:

pandoc -s -o doc.html part01.md part02.md
pandoc -s -o doc.docx part01.md part02.md

* pandoc --from=markdown --to=rst --output=README.rst README.md
