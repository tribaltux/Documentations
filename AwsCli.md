# AWS CLI

## Installer awscli
`apt-get install awscli`

## AutoCompletion aws
```
whereis aws_completer
aws_completer: /usr/bin/aws_completer
Placer le complete dans le .bashrc
complete -C '/usr/bin/aws_completer' aws
```

## Configurer awscli
`aws configure`
> Renseigner les ID

## BUCKET - S3 - S3API

### Créer un bucket privé
`aws s3api create-bucket --bucket bucket-test-admin-gqu --region eu-west-1 --create-bucket-configuration LocationConstraint=eu-west-1`

ou

` aws s3 mb s3://bucket-test-admin-gqu`

### Créer un bucket public
`aws s3api create-bucket --bucket bucket-test-admin-gqu --acl public-read --region eu-west-1 --create-bucket-configuration LocationConstraint=eu-west-1`

ou

`aws s3 mb s3://bucket-test-admin-gqu`

### Uploader un fichier
`aws s3 cp "TPAws.txt" s3://bucket-test-admin-gqu/`

### Uploader un fichier Public
`aws s3 cp "TPAws.txt" s3://bucket-test-admin-gqu/ --acl public-read`

### Supprimer un fichier
`aws s3 rm s3://bucket-test-admin-gqu/TPAws.txt`

### Supprimer un bucket
`aws s3 rb s3://bucket-test-admin-gqu`

ou

`aws s3api delete-bucket --bucket bucket-test-admin-gqu`

## EC2

### Voir les instances du profile awsprofile
`aws --profile awsprofile ec2 describe-instances`

### Voir les clefs ssh associées
`aws --profile awsprofile ec2 describe-key-pairs`

### Voir les images de Debian ( identifiant 379101102735 )
`aws --profile awsprofile ec2 describe-images --owners 379101102735`

### Voir les images de Debian de version debian-jessie ainsi que la date de création de l'identifiant Debian
`aws --profile awsprofile ec2 describe-images --owners 379101102735 --filters 'Name=name,Values=debian-jessie-*' --query 'Images[].[CreationDate,ImageId]'`

### Créer une instance de type t2.micro avec ma clef  et l'image choisie ImageId
`aws --profile awsprofile ec2 run-instances --count 1 --instance-type t2.micro --key-name admin_gqu --associate-public-ip-address --image-id ami-402f1a33`

### Associer un Tag à l'instance
`aws --profile awsprofile ec2 create-tags --resources i-0df998bb51a317159 --tags Key=Name,Value=TestAwsCliGQU`

### Creer un groupe de sécurité
`aws --profile awsprofile ec2 create-security-group --group-name SSHOnlyOT --description 'Authorize only SSH from OT'`

### Associer la règle au groupe de sécurité
`aws --profile awsprofile ec2 authorize-security-group-ingress --group-name SSHOnlyOT --protocol tcp --port 22 --cidr '80.74.64.33/24'`

### Lister les règles du groupe de sécu
`aws --profile awsprofile ec2 describe-security-groups --group-names SSHOnlyOT`

### Rattacher le groupe à l'instance
`aws --profile awsprofile ec2 modify-instance-attribute --instance-id i-0df998bb51a317159 --groups sg-e638279e`

### Stopper une instance avec son ID
`aws --profile awsprofile ec2 stop-instances --instance-ids i-0df998bb51a317159`

### Terminate ( Supprimer ) une instance
`aws --profile awsprofile ec2 terminate-instances --instance-id i-0df998bb51a317159`

### Lister les VPCS
`aws --profile awsprofile ec2 describe-vpcs`

### Lister les VPCS Peering
`aws --profile awsprofile ec2 describe-vpc-peering-connections`
