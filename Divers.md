# Sysdig
## Install
```
curl -s https://s3.amazonaws.com/download.draios.com/stable/install-sysdig | sudo bash
```
## Utilisation
* All logs
```
sysdig -c spy_logs
```
* All activity from users
```
sysdig -c spy_users
```
# TcpDump
## Utilisation
* On specific port
```
tcpdump -A -s0 -vvv -n port 80
```
* On specific port and specific address
```
tcpdump -A -s0 -vvv -n port 80 and host X.X.X.X
```

# Curl
## Utilisation
* Basic
```
curl http://www.google.com
```
* Print Return Code `-i`
```
curl -i http://www.google.com
```
* Follow Redirects `-L`
```
curl -iL http://www.google.com
```
* Download Files `-O`
```
curl -O http://www.google.com/index.html
```
* Rename File Name `-o`
```
curl -o bleu.html http://www.google.com/index.html
```
* Verbose Mode `--verbose`
```
curl --verbose -i http://wwww.google.fr
```
* Change HTTP Request Method `-X`
```
curl -X POST http://bleu.com/Test
```
* Change Header `-H`
```
curl -X POST -H "Content-Type:text/xml;charset=UTF-8" http://bleu.com/....
```
* Include Certificate `--cert` and `--key`
```
curl --verbose --cert /etc/ssl/bleu.cer --key /etc/ssl/bleu.key  "https://bleu.com/API"
```
* Force DNS `--resolve`
```
curl --resolve rouge.com:449:10.2.0.1 --verbose "https://bleu.com/API"
```
* Silent Mode `-s` `--silent`
```
curl -s http://bleu.com
```
* Upload File
```
curl -v -X POST -F "uploaded=@test.xml" https://bleu.com/Upload
```

# Tmux
## Utilisation

* List sessions
```
tmux ls
```
* Start session with name Test
```
tmux new -s Test
```
* Attach to the Test session
```
tmux attach -t Test
```
* Kill session Test
```
tmux kill-session -t Test
```
* In a tmux session
  * Split vertically
  ```
  CTRB+B %
  ```
  * Split horizontaly
  ```
  CTRB+B "
  ```
  * Back in history console
  ```
  CTRB+B [ or page up
  ```
  * Full screen window
  ```
  CTRB+B z
  ```
  * Create a new window
  ```
  CTRB+B
  ```
  * Go to the next window
  ```
  CTRB+B n
  ```
  * Detach tmux session
  ```
  CTRB+B c
  ```
  * Rename Windows
  ```
  CTRB+B ,
  ```

