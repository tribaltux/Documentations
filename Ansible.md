# ANSIBLE

## Pré-requis
Ansible installation

http://docs.ansible.com/ansible/latest/intro_installation.html#latest-releases-via-apt-debian
```
Latest Releases Via Apt (Debian)
Debian users may leverage the same source as the Ubuntu PPA.

Add the following line to /etc/apt/sources.list:

deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main
Then run these commands:

$ sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
$ sudo apt-get update
$ sudo apt-get install ansible
Note

This method has been verified with the Trusty sources in Debian Jessie and Stretch but may not be supported in earlier versions.
```

## Explications
1. __Files :__

* ansible.cfg
> Configuration file for ansible.

* hosts  
> Servers Lists or Servers Groups

```
[groupefront]
front1
front2

[groupeback]
back1
back2

[all]
front1
front2
back1
back2
```

2. __Folders :__

* roles
> Contain yaml files

* files
> Contain use in the yaml files

## Utilisation basique

### Lancement d'une "recette" :

**ATTENTION** : n'oubliez pas de vérifier les déclarations dans le fichier `hosts` !

Exemple pour tester :

```
ansible-playbook -i hosts --check --diff roles/create_admin.yml
```

Exemple pour lancer :

```
ansible-playbook -i hosts roles/create_admin.yml
