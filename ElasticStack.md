{{toc/}}

# Install ELK on Deb9

## Server Side

### First Upgrade your debian

    apt-get update && apt-get dist-upgrade

### Install Java - OpenJDK 8

* Install

```
apt-get install openjdk-8-jdk
```

* Test

```
java -version
```

### Install Elasticsearch

_The indexation motor_

* Enable Official Repository

```
curl -O https://artifacts.elastic.co/GPG-KEY-elasticsearch
apt-key add GPG-KEY-elasticsearch
echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" > /etc/apt/sources.list.d/elastic-6.x.list
```

* Update

```
apt-get update
```

* Install

```
apt-get install elasticsearch
```

* Configuration

Bind address

```
vi /etc/elasticsearch/elasticsearch.yml
```

```
...
network.host: <IP> #IP si non local, sinon localhost
transport.host: <IP>
...
```

Adapt JVM Size with yours resources

```
vi /etc/elasticsearch/jvm.options
```

```
...
# Xms represents the initial size of total heap space
# Xmx represents the maximum size of total heap space

-Xms500m
-Xmx750m
...
```

* Reload systemd

```
systemctl daemon-reload
```

* Enable Elasticsearch on boot

```
systemctl enable elasticsearch.service
```

* Start Elasticsearch

```
systemctl start elasticsearch.service
```

* Test status of ElasticSearch server

```
systemctl status elasticsearch.service
```

and

```
curl -X GET http://localhost:9200
```

```)
{
  "name" : "ZVu5vwt",
  "cluster_name" : "elasticsearch",
  "cluster_uuid" : "lNkepxMwT6GmBKFjhcIL7g",
  "version" : {
    "number" : "6.2.4",
    "build_hash" : "ccec39f",
    "build_date" : "2018-04-12T20:37:28.497551Z",
    "build_snapshot" : false,
    "lucene_version" : "7.2.1",
    "minimum_wire_compatibility_version" : "5.6.0",
    "minimum_index_compatibility_version" : "5.0.0"
  },
  "tagline" : "You Know, for Search"
}
```
* Install Elasticsearch GeoIP and user agent plugins
```
/usr/share/elasticsearch/bin/elasticsearch-plugin install ingest-geoip
/usr/share/elasticsearch/bin/elasticsearch-plugin install ingest-user-agent
```
### Install Kibana

_The interface_

* Install

```
apt-get install kibana
```

* Configuration

```
vi /etc/kibana/kibana.yml
```

```
# Kibana is served by a back end server. This setting specifies the port to use.
server.port: <PORT> # Default 5601

# Specifies the address to which the Kibana server will bind. IP addresses and host names are both valid values.
# The default is 'localhost', which usually means remote machines will not be able to connect.
# To allow connections from remote users, set this parameter to a non-loopback address.
server.host: "<IP>"
...
# The URL of the Elasticsearch instance to use for all your queries.
elasticsearch.url: "http://<IP_ELASTICSEARCH>:9200"
...
```

* Reload systemd

```
systemctl daemon-reload
```

* Enable Kibana on boot

```
systemctl enable kibana.service
```

* Start Kibana

```
systemctl start kibana.service
```

* Test status of Kibana server

```
systemctl status kibana.service
```
## Client Side

### Install Filebeat

_The Push Flux_

* Enable Official Repository

```
curl -O https://artifacts.elastic.co/GPG-KEY-elasticsearch
apt-key add GPG-KEY-elasticsearch
echo "deb https://artifacts.elastic.co/packages/6.x/apt stable main" > /etc/apt/sources.list.d/elastic-6.x.list
```

* Update

```
apt-get update
```

* Install

```
apt-get install filebeat
```

* Configuration

```
mv /etc/filebeat/filebeat.yml /etc/filebeat/ORIG_filebeat.yml && vi /etc/filebeat/filebeat.yml
```

```
...
filebeat.prospectors:
- type: log
  enabled: false
  paths:
    - /var/log/*.log
...
filebeat.config.modules:
  path: ${path.config}/modules.d/*.yml
  reload.enabled: false
...
setup.template.settings:
  index.number_of_shards: 3
...
setup.kibana:
  host: "<IP_KIBANA>:5601"
...
output.elasticsearch:
  hosts: ["<IP_ELASTICSEARCH:9200"]
```

* Enable filebeat Apache and System modules
```
filebeat modules enable apache2
filebeat modules enable system
```

* Load Kibana Dashbaord
```
filebeat setup
```

* Enable Filebeat on boot

```
systemctl enable filebeat.service
```

* Start Filebeat

```
systemctl start filebeat.service
```

* Test status of filebeat server

```
systemctl status filebeat.service
```
* Advanced Configurations files
```
/etc/filebeat/modules./{apache2,system}.yml
```

### Install MetricBeat

* Install
```
apt-get install metricbeat
```

* Configuration

```
mv /etc/metricbeat/metricbeat.yml /etc/metricbeat/ORIG_metricbeat.yml && vi /etc/metricbeat/metricbeat.yml
```
```
...
metricbeat.config.modules:
  path: ${path.config}/modules.d/*.yml
  reload.enabled: false
...
setup.template.settings:
  index.number_of_shards: 1
  index.codec: best_compression
...
setup.kibana:
  host: "<IP_KIBANA>:5601"
...
output.elasticsearch:
  hosts: ["<IP_ELASTICSEARCH:9200"]
```

* Enable metricbeat Apache and System modules
```
metricbeat modules enable apache2
metricbeat modules enable system
```

* Load Kibana Dashbaord
```
metricbeat setup
```

* Enable Metricbeat on boot

```
systemctl enable metricbeat.service
```

* Start Metricbeat

```
systemctl start metricbeat.service
```

* Test status of Metricbeat server

```
systemctl status metricbeat.service
```
* Advanced Configurations files
```
/etc/metricbeat/modules./{apache2,system}.yml
```
